package uk.sliske.web.service.dg;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uk.sliske.web.service.Constants;
import uk.sliske.web.service.PriceHandler;

/**
 * Servlet implementation class DGPriceHandler
 */
@WebServlet("/DG")
public class DGPriceHandler extends PriceHandler {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DGPriceHandler() {
		super();
		prices.put(0, 75);
		prices.put(50, 50);
		prices.put(70, 26);
		prices.put(85, 23);
		prices.put(99, 20);
		prices.put(110, 18);

	}

	private float calcCombatModifier(int combatLevel) {
		 float temp1 = ((float) combatLevel) / 138.0f;
		 float temp2 = 1.0f / temp1;
		 float temp3 = temp2 - 1;
		 float temp4 = temp3 / 3.0f;
		 return temp4 + 1;
		//return 1;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Map<String, String[]> params = request.getParameterMap();
		PrintWriter out = response.getWriter();
		String title = "DG Price";
		out.println("<!DOCTYPE html>\n<html>\n<head>");
		out.println("	<link rel=\"stylesheet\" href=\""+Constants.STYLESHEET_URL+"\">");
		out.printf("	<title>%s</title>\n",title);
		out.println("</head>");
		out.println("<body background=\"http://sliske.uk/img/captures/d134d8.png\" class=\"dg\">");
		out.println("	<text-1>");
		int startLvl = 0;
		int endLvl = 0;
		int combatLvl = 0;
		int basePrice = 0;
		float modifier = 1;
		boolean ironman = false;
		try {
			startLvl = Integer.parseInt(params.get("start")[0]);
			endLvl = Integer.parseInt(params.get("end")[0]);
			basePrice = getBasePrice(startLvl, endLvl - 1);
			combatLvl = Integer.parseInt(params.get("combat")[0]);
			if (params.containsKey("iron")) {
				if (params.get("iron")[0].equalsIgnoreCase("yes")) {
					ironman = true;
					modifier *= 1.5f;
				}
			}
			modifier *= calcCombatModifier(combatLvl);
		} catch (Exception e) {

		}
		int price = (int) (((float) basePrice) * modifier);
		if (price <= 0)
			out.println("		error\n");
		else {
			out.printf("		combat level : %d\n", combatLvl);
			if (ironman)
				out.printf("		<br>ironman mode enabled\n");
			out.printf(
					"		<br>Price for dungeoneering from level %d to level %d : \n",
					startLvl, endLvl);
			out.println("		"+formatPrice("###,###,###,###", price));

		}
		out.println("	</text-1>");
		out.println("</body>");
		out.println("</html>");
		response.flushBuffer();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	

}
