package uk.sliske.web.service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PriceHandler
 */
public abstract class PriceHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected HashMap<Integer, Integer> prices = new HashMap<>();

	protected int[] xpCosts = new int[Skill.XPS.length];

	protected int[] costs = new int[Skill.XPS.length];
	
	private boolean pricesSet = false;

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PriceHandler() {
        super();
    }

    protected final void finishInit(){
		for (Integer i : prices.keySet()) {
			for (int j = i.intValue(); j < xpCosts.length; j++) {
				xpCosts[j] = prices.get(i);
			}
		}
		for (int i = 0; i < xpCosts.length; i++) {
			costs[i] = xpCosts[i] * Skill.XPS[i];
		}
    }
    
    protected int getBasePrice(int startLvl, int endLvl) {
    	checkPrice();
		int res = 0;
		for (int i = startLvl; i < endLvl; i++) {
			res += costs[i];
		}
		return res;
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private void checkPrice(){
		if(!pricesSet){
			finishInit();
		}
	}
	
	protected static String formatPrice(String pattern, double value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}

}
